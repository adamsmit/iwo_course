#!/bin/bash

# How many times does the word 'de' occur in the text file.
function wordcount {
    	echo "${FUNCNAME[0]}"
		grep -Eo '\w+' RUG_wiki_page.txt | grep -wi de | wc -l
}
